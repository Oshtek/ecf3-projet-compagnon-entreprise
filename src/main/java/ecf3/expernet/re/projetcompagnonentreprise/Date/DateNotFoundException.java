// Classe relevant une exception en cas d'action effectuée sur un Contact inexistant

package ecf3.expernet.re.projetcompagnonentreprise.Date;

class DateNotFoundException extends RuntimeException {

	DateNotFoundException(Long id) {
		super("Le contact " + id + " est introuvable");
	}
}