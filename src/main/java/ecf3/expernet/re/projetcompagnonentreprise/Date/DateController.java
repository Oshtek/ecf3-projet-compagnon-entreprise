/* Le controller est une composante essentielle :
 * Il permet de gérer les différentes requetes HTTP (GET / POST / PUT / DELETE) réalisées sur notre application
 */

package ecf3.expernet.re.projetcompagnonentreprise.Date;


/********* Import des librairies nécesssaires au fonctionnement du projet ************/
import java.util.List;
import javax.transaction.Transactional;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Transactional //Autorise les transactions avec la database
@RestController //Controller utilisant l'API REST
class DateController {

  private final DateRepository repository;

  DateController(DateRepository repository) {
    this.repository = repository;
  }


  // [CRUD : READ] Lister l'ensemble des dates déclarés dans la table 
  @GetMapping("/Dates")
  @CrossOrigin(origins = "*")
  List<Date> all() {
    return (List<Date>) repository.findAll();
  }

  // [CRUD : CREATE] Créer un nouvel date dans la table
  @PostMapping("/CreateDate")
  Date newDate(@RequestBody Date newDate) {
    return repository.save(newDate);
  }

  // [CRUD : READ] Lister un date spécifique
  @GetMapping("/Date/{id}")
  Date one(@PathVariable Long id) {

    return repository.findById(id)
      .orElseThrow(() -> new DateNotFoundException(id));
  }

  // [CRUD : UPDATE] Modifier les informations d'un date
  @PutMapping("/UpdateDate/{id}")
  Date replaceDate(@RequestBody Date newDate, @PathVariable Long id) {

    return repository.findById(id)
      .map(Date -> {
        Date.setDateLecture(newDate.getDateLecture());
        Date.setDateEnvoi(newDate.getDateEnvoi());
        return repository.save(Date);
      })
      .orElseGet(() -> {
        newDate.setId(id);
        return repository.save(newDate);
      });
  }

//[CRUD : DELETE] Supprimer un date de la table
  @DeleteMapping("/Date/{id}")
  public void deleteDate(@PathVariable Long id) {
    repository.deleteById(id);
  }
  
  
}