// Définition de la classe Contact
package ecf3.expernet.re.projetcompagnonentreprise.Date;

/********* Import des librairies nécesssaires au fonctionnement du projet ************/
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity //Ce mot cle indique à Hibernate de creer un tableau dans MySQL à partir de cette classe
@Table(name = "Historique")
public class Date {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)

	@Column(name = "idDate")
	private Long id;
	
	private java.util.Date dateLecture;

	private java.util.Date dateEnvoi;

	/********* Réalisation des Getters et Setter pour les interactions avec les attributs ************/

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public java.util.Date getDateEnvoi() {
		return dateEnvoi;
	}

	public void setDateEnvoi(java.util.Date dateEnvoi) {
		this.dateEnvoi = dateEnvoi;
	}

	public java.util.Date getDateLecture() {
		return dateLecture;
	}

	public void setDateLecture(java.util.Date dateLecture) {
		this.dateLecture = dateLecture;
	}

}