/* Le controller est une composante essentielle :
 * Il permet de gérer les différentes requetes HTTP (GET / POST / PUT / DELETE) réalisées sur notre application
 */

package ecf3.expernet.re.projetcompagnonentreprise.Conversation;


/********* Import des librairies nécesssaires au fonctionnement du projet ************/
import java.util.List;
import javax.transaction.Transactional;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ecf3.expernet.re.projetcompagnonentreprise.Message.Message;
import ecf3.expernet.re.projetcompagnonentreprise.Message.MessageRepository;

@Transactional //Autorise les transactions avec la database
@RestController //Controller utilisant l'API REST
class ConversationController {

  private final ConversationRepository repository;
  private final MessageRepository messages;

  ConversationController(ConversationRepository repository, MessageRepository messages) {
    this.repository = repository;
	this.messages = messages;
  }


  // [CRUD : READ] Lister l'ensemble des conversations déclarées dans la table 
  @GetMapping("/Conversations")
  @CrossOrigin(origins = "*")
  List<Conversation> all() {
    return (List<Conversation>) repository.findAll();
  }

  // [CRUD : CREATE] Créer une nouvelle conversation dans la table
  @PostMapping("/CreateConversation")
  Conversation newConversation(@RequestBody Conversation newConversation) {
    return repository.save(newConversation);
  }

  // [CRUD : READ] Lister un utilisateur spécifique
  @GetMapping("/Conversation/{id}")
  Conversation one(@PathVariable Long id) {
	  
	  //Test ajout message à la conversation
	  Conversation tmp = new Conversation();
	  Message message1 = messages.findById(1L).get();
	  Message message2 = messages.findById(2L).get();
	  tmp.getMessage().add(message1);
	  tmp.getMessage().add(message2);
	  repository.save(tmp);
	  
    return repository.findById(id)
      .orElseThrow(() -> new ConversationNotFoundException(id));
  }

  // [CRUD : UPDATE] Modifier les informations d'un utilisateur
  @PutMapping("/UpdateConversation/{id}")
  Conversation replaceConversation(@RequestBody Conversation newConversation, @PathVariable Long id) {

    return repository.findById(id)
      .map(Conversation -> {
        Conversation.setMessage(newConversation.getMessage());
        Conversation.setNbParticipant(newConversation.getNbParticipant());
        return repository.save(Conversation);
      })
      .orElseGet(() -> {
        newConversation.setId(id);
        return repository.save(newConversation);
      });
  }

//[CRUD : DELETE] Supprimer un utilisateur de la table
  @DeleteMapping("/Conversation/{id}")
  public void deleteConversation(@PathVariable Long id) {
    repository.deleteById(id);
  }
  
  
}