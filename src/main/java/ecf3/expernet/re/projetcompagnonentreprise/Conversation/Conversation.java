// Définition de la classe Conversation
package ecf3.expernet.re.projetcompagnonentreprise.Conversation;

import java.util.HashSet;
import java.util.Set;

/********* Import des librairies nécesssaires au fonctionnement du projet ************/
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/********* Import des classes ************/
import ecf3.expernet.re.projetcompagnonentreprise.Message.Message;

@Entity //Ce mot cle indique à Hibernate de creer un tableau dans MySQL à partir de cette classe
@Table(name = "Conversations")
public class Conversation {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "idConversation")
	private Long id;
	
	@OneToMany
	private Set<Message> message;

	private Long nbParticipant;

	/********* Réalisation des Getters et Setter pour les interactions avec les attributs ************/
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getNbParticipant() {
		return nbParticipant;
	}

	public void setNbParticipant(Long nbParticipant) {
		this.nbParticipant = nbParticipant;
	}

	public Set<Message> getMessage() {
		if (message == null) {
			message = new HashSet<Message>();
		}
		return message;
	}

	public void setMessage(Set<Message> message) {
		this.message = message;
	}


}