// Classe relevant une exception en cas d'action effectuée sur un Contact inexistant

package ecf3.expernet.re.projetcompagnonentreprise.Conversation;

class ConversationNotFoundException extends RuntimeException {

	ConversationNotFoundException(Long id) {
		super("Le contact " + id + " est introuvable");
	}
}