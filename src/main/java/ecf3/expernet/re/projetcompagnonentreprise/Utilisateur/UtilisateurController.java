/* Le controller est une composante essentielle :
 * Il permet de gérer les différentes requetes HTTP (GET / POST / PUT / DELETE) réalisées sur notre application
 */

package ecf3.expernet.re.projetcompagnonentreprise.Utilisateur;


/********* Import des librairies nécesssaires au fonctionnement du projet ************/
import java.util.List;
import javax.transaction.Transactional;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Transactional //Autorise les transactions avec la database
@RestController //Controller utilisant l'API REST
class UtilisateurController {

  private final UtilisateurRepository repository;

  UtilisateurController(UtilisateurRepository repository) {
    this.repository = repository;
  }


  // [CRUD : READ] Lister l'ensemble des utilisateurs déclarés dans la table 
  @GetMapping("/Utilisateurs")
  @CrossOrigin(origins = "*")
  List<Utilisateur> all() {
    return (List<Utilisateur>) repository.findAll();
  }

  // [CRUD : CREATE] Créer un nouvel utilisateur dans la table
  @PostMapping("/CreateUtilisateur")
  Utilisateur newUtilisateur(@RequestBody Utilisateur newUtilisateur) {
    return repository.save(newUtilisateur);
  }

  // [CRUD : READ] Lister un utilisateur spécifique
  @GetMapping("/Utilisateur/{id}")
  Utilisateur one(@PathVariable Long id) {
    return repository.findById(id)
      .orElseThrow(() -> new UtilisateurNotFoundException(id));
  }

  // [CRUD : UPDATE] Modifier les informations d'un utilisateur
  @PutMapping("/UpdateUtilisateur/{id}")
  Utilisateur replaceUtilisateur(@RequestBody Utilisateur newUtilisateur, @PathVariable Long id) {

    return repository.findById(id)
      .map(Utilisateur -> {
        Utilisateur.setNom(newUtilisateur.getNom());
        Utilisateur.setPrenom(newUtilisateur.getPrenom());
        Utilisateur.setEmail(newUtilisateur.getEmail());
        Utilisateur.setIdentifiant(newUtilisateur.getIdentifiant());
        Utilisateur.setMotDePasse(newUtilisateur.getMotDePasse());
        Utilisateur.setStatus(newUtilisateur.getStatus());
        return repository.save(Utilisateur);
      })
      .orElseGet(() -> {
        newUtilisateur.setId(id);
        return repository.save(newUtilisateur);
      });
  }

//[CRUD : DELETE] Supprimer un utilisateur de la table
  @DeleteMapping("/Utilisateur/{id}")
  public void deleteUtilisateur(@PathVariable Long id) {
    repository.deleteById(id);
  }
  
  
}