// Définition de la classe Contact
package ecf3.expernet.re.projetcompagnonentreprise.Utilisateur;

/********* Import des librairies nécesssaires au fonctionnement du projet ************/
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity //Ce mot cle indique à Hibernate de creer un tableau dans MySQL à partir de cette classe
@Table(name = "Utilisateurs")
public class Utilisateur {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)

	@Column(name = "idUtilisateur")
	private Long id;

	private String identifiant;

	private String motDePasse;
	
	private String nom;
	
	private String prenom;

	private String email;

	private String status;

	/********* Réalisation des Getters et Setter pour les interactions avec les attributs ************/

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIdentifiant() {
		return identifiant;
	}

	public void setIdentifiant(String identifiant) {
		this.identifiant = identifiant;
	}

	public String getMotDePasse() {
		return motDePasse;
	}

	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

}