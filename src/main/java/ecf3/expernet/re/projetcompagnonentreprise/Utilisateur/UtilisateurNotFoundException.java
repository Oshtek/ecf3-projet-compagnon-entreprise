// Classe relevant une exception en cas d'action effectuée sur un Contact inexistant

package ecf3.expernet.re.projetcompagnonentreprise.Utilisateur;

class UtilisateurNotFoundException extends RuntimeException {

	UtilisateurNotFoundException(Long id) {
		super("Le contact " + id + " est introuvable");
	}
}