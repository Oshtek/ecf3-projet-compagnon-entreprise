/* Le controller est une composante essentielle :
 * Il permet de gérer les différentes requetes HTTP (GET / POST / PUT / DELETE) réalisées sur notre application
 */

package ecf3.expernet.re.projetcompagnonentreprise.Contact;


/********* Import des librairies nécesssaires au fonctionnement du projet ************/
import java.util.List;
import javax.transaction.Transactional;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Transactional //Autorise les transactions avec la database
@RestController //Controller utilisant l'API REST
class ContactController {

  private final ContactRepository repository;

  ContactController(ContactRepository repository) {
    this.repository = repository;
  }


  // [CRUD : READ] Lister l'ensemble des contacts déclarés dans la table 
  @GetMapping("/Contacts")
  @CrossOrigin(origins = "*")
  List<Contact> all() {
    return (List<Contact>) repository.findAll();
  }

  // [CRUD : CREATE] Créer un nouvel contact dans la table
  @PostMapping("/CreateContact")
  Contact newContact(@RequestBody Contact newContact) {
    return repository.save(newContact);
  }

  // [CRUD : READ] Lister un contact spécifique
  @GetMapping("/Contact/{id}")
  Contact one(@PathVariable Long id) {

    return repository.findById(id)
      .orElseThrow(() -> new ContactNotFoundException(id));
  }

  // [CRUD : UPDATE] Modifier les informations d'un contact
  @PutMapping("/UpdateContact/{id}")
  Contact replaceContact(@RequestBody Contact newContact, @PathVariable Long id) {

    return repository.findById(id)
      .map(Contact -> {
        Contact.setNom(newContact.getNom());
        Contact.setPrenom(newContact.getPrenom());
        Contact.setPseudo(newContact.getPseudo());
        Contact.setStatus(newContact.getStatus());
        return repository.save(Contact);
      })
      .orElseGet(() -> {
        newContact.setId(id);
        return repository.save(newContact);
      });
  }

//[CRUD : DELETE] Supprimer un contact de la table
  @DeleteMapping("/Contact/{id}")
  public void deleteContact(@PathVariable Long id) {
    repository.deleteById(id);
  }
  
  
}