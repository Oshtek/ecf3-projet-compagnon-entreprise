// Classe relevant une exception en cas d'action effectuée sur un Contact inexistant

package ecf3.expernet.re.projetcompagnonentreprise.Contact;

class ContactNotFoundException extends RuntimeException {

	ContactNotFoundException(Long id) {
		super("Le contact " + id + " est introuvable");
	}
}