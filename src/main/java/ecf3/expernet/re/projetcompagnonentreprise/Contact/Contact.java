// Définition de la classe Contact
package ecf3.expernet.re.projetcompagnonentreprise.Contact;

/********* Import des librairies nécesssaires au fonctionnement du projet ************/
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity //Ce mot cle indique à Hibernate de creer un tableau dans MySQL à partir de cette classe
@Table(name = "Contacts")
public class Contact {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)

	@Column(name = "idContact")
	private Long id;

	private String nom;

	private String prenom;

	private String pseudo;

	private String status;

	/********* Réalisation des Getters et Setter pour les interactions avec les attributs ************/

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


}