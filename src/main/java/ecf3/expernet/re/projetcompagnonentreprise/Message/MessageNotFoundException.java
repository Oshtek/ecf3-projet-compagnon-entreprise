// Classe relevant une exception en cas d'action effectuée sur un Contact inexistant

package ecf3.expernet.re.projetcompagnonentreprise.Message;

class MessageNotFoundException extends RuntimeException {

	MessageNotFoundException(Long id) {
		super("Le contact " + id + " est introuvable");
	}
}