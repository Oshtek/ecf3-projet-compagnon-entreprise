/* Le controller est une composante essentielle :
 * Il permet de gérer les différentes requetes HTTP (GET / POST / PUT / DELETE) réalisées sur notre application
 */

package ecf3.expernet.re.projetcompagnonentreprise.Message;


/********* Import des librairies nécesssaires au fonctionnement du projet ************/
import java.util.List;
import javax.transaction.Transactional;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import ecf3.expernet.re.projetcompagnonentreprise.Utilisateur.Utilisateur;

@Transactional //Autorise les transactions avec la database
@RestController //Controller utilisant l'API REST
class MessageController {

  private final MessageRepository repository;

  MessageController(MessageRepository repository) {
    this.repository = repository;
  }


  // [CRUD : READ] Lister l'ensemble des messages déclarés dans la table 
  @GetMapping("/Messages")
  @CrossOrigin(origins = "*")
  List<Message> all() {
    return (List<Message>) repository.findAll();
  }

  // [CRUD : CREATE] Créer un nouvel message dans la table
  @PostMapping("/CreateMessage")
  Message newMessage(@RequestBody Message newMessage) {
    return repository.save(newMessage);
  }

  // [CRUD : READ] Lister un message spécifique
  @GetMapping("/Message/{id}")
  Message one(@PathVariable Long id) {
	  
	  //Test création message
	  	Message message1 = new Message();
		message1.setContenu("yo");
		Message message2 = new Message();
		message2.setContenu("Salut, ça va ?");
		repository.save(message1);
		repository.save(message2);
		
    return repository.findById(id)
      .orElseThrow(() -> new MessageNotFoundException(id));
  }

  // [CRUD : UPDATE] Modifier les informations d'un message
  @PutMapping("/UpdateMessage/{id}")
  Message replaceMessage(@RequestBody Message newMessage, @PathVariable Long id) {

    return repository.findById(id)
      .map(Message -> {
        Message.setContenu(newMessage.getContenu());
        Message.setLu(newMessage.getLu());
        Message.setUrlFichier(newMessage.getUrlFichier());
        return repository.save(Message);
      })
      .orElseGet(() -> {
        newMessage.setId(id);
        return repository.save(newMessage);
      });
  }

//[CRUD : DELETE] Supprimer un message de la table
  @DeleteMapping("/Message/{id}")
  public void deleteMessage(@PathVariable Long id) {
    repository.deleteById(id);
  }
  
  
}