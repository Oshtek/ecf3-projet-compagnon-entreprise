// Définition de la classe Contact
package ecf3.expernet.re.projetcompagnonentreprise.Message;

/********* Import des librairies nécesssaires au fonctionnement du projet ************/
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ecf3.expernet.re.projetcompagnonentreprise.Conversation.Conversation;

@Entity //Ce mot cle indique à Hibernate de creer un tableau dans MySQL à partir de cette classe
@Table(name = "Messages")
public class Message {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)

	@Column(name = "idMessage")
	private Long id;
	
	@ManyToOne
	private Conversation conversation;

	private String contenu;

	private Boolean lu;

	private String urlFichier;

	/********* Réalisation des Getters et Setter pour les interactions avec les attributs ************/
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getContenu() {
		return contenu;
	}

	public void setContenu(String contenu) {
		this.contenu = contenu;
	}

	public Boolean getLu() {
		return lu;
	}

	public void setLu(Boolean lu) {
		this.lu = lu;
	}

	public String getUrlFichier() {
		return urlFichier;
	}

	public void setUrlFichier(String urlFichier) {
		this.urlFichier = urlFichier;
	}
	
	public Conversation getConversation() {
		return conversation;
	}

	public void setConversation(Conversation conversation) {
		this.conversation = conversation;
	}

}