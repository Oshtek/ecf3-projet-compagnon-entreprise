package ecf3.expernet.re.projetcompagnonentreprise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetCompagnonEntrepriseApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetCompagnonEntrepriseApplication.class, args);
	}

}
