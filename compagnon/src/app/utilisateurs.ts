export class Utilisateurs {

  public id: number;

  public identifiant: string;

  public motDePasse: string;

  public nom: string;

  public prenom: string;

  public email: string;

  public status: string;


  public get Id(): number {
    return this.id;
  }

  public set Id(value: number) {
    this.id = value;
  }

  public get Identifiant(): string {
    return this.identifiant;
  }

  public set Identifiant(value: string) {
    this.identifiant = value;
  }

  public get MotDePasse(): string {
    return this.motDePasse;
  }

  public set MotDePasse(value: string) {
    this.motDePasse = value;
  }

  public get Nom(): string {
    return this.nom;
  }

  public set Nom(value: string) {
    this.nom = value;
  }

  public get Prenom(): string {
    return this.prenom;
  }

  public set Prenom(value: string) {
    this.prenom = value;
  }

  public get Email(): string {
    return this.email;
  }

  public set Email(value: string) {
    this.email = value;
  }

  public get Status(): string {
    return this.status;
  }

  public set Status(value: string) {
    this.status = value;
  }


  public toString(): string {
    return '{ \n id : ' + this.id + '; \n identifiant : ' + this.identifiant + '; \n motDePasse : ' + this.motDePasse +
    '; \n nom : ' + this.nom + '; \n prenom : ' + this.prenom + '; \n email : ' + this.email + '; \n status : ' + this.status + '; \n }';
}
}
