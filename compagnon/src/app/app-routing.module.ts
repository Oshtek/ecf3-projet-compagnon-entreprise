import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConversationComponent } from './conversation/conversation.component';
import { ContactComponent } from './contact/contact.component';
import { DateComponent } from './date/date.component';
import { MessageComponent } from './message/message.component';
import { UtilisateursComponent } from './utilisateurs/utilisateurs.component';

const routes: Routes = [
  { path: 'Conversations', component: ConversationComponent },
  { path: 'Contact', component: ContactComponent },
  { path: 'Date', component: DateComponent },
  { path: 'Message', component: MessageComponent },
  { path: 'Utilisateurs', component: UtilisateursComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
