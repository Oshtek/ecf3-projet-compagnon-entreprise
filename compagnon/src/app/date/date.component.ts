import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Date} from './../date';



@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.css']
})
export class DateComponent implements OnInit {

  listeDateURL = 'http://localhost:8080/Dates';

  value: Date[];

  constructor(private _httpClient: HttpClient) { }

  ngOnInit(): void {
    this._httpClient.get<Date[]>(this.listeDateURL).subscribe(value => this.value = value);
  }

}
