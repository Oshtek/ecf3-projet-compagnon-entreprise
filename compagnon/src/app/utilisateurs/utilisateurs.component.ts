import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Utilisateurs} from './../utilisateurs';


@Component({
  selector: 'app-utilisateurs',
  templateUrl: './utilisateurs.component.html',
  styleUrls: ['./utilisateurs.component.css']
})
export class UtilisateursComponent implements OnInit {

  listeUtilisateursURL = 'http://localhost:8080/Utilisateurs';

  value: Utilisateurs[];

  constructor(private _httpClient: HttpClient) { }

  ngOnInit(): void {
    this._httpClient.get<Utilisateurs[]>(this.listeUtilisateursURL).subscribe(value => this.value = value);
  }

}
