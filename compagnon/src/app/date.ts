export class Date {

  public id: number;

  public dateLecture: string;

  public dateEnvoi: string;


  public get Id(): number {
    return this.id;
  }

  public set Id(value: number) {
    this.id = value;
  }

  public get DateLecture(): string {
    return this.dateLecture;
  }

  public set DateLecture(value: string) {
    this.dateLecture = value;
  }

  public get DateEnvoi(): string {
    return this.dateEnvoi;
  }

  public set DateEnvoi(value: string) {
    this.dateEnvoi = value;
  }

  public toString(): string {
    return '{ \n id : ' + this.id + '; \n Lu le : ' + this.dateLecture + '; \n envoyé le : ' + this.dateEnvoi + '; \n }';
}

}
