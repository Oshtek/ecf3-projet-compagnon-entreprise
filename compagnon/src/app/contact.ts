export class Contact {

  private id: number;

  private nom: string;

  public prenom: string;

  public pseudo: string;

  public status: string;


  public get Id(): number {
    return this.id;
  }
  public set Id(value: number) {
    this.id = value;
  }

  public get Nom(): string {
    return this.nom;
  }
  public set Nom(value: string) {
    this.nom = value;
  }

  public get Prenom(): string {
    return this.prenom;
  }

  public set Prenom(value: string) {
    this.prenom = value;
  }

  public get Pseudo(): string {
    return this.pseudo;
  }

  public set Pseudo(value: string) {
    this.pseudo = value;
  }

  public get Status(): string {
    return this.status;
  }

  public set Status(value: string) {
    this.status = value;
  }

  public toString(): string {
    return '{ \n id : ' + this.id + '; \n nom : ' + this.nom + '; \n prenom : ' + this.prenom +
    '; \n pseudo : ' + this.pseudo + '; \n status : ' + this.status + '; \n }';
}

}
