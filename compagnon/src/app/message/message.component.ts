import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Message} from './../message';


@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  listeMessageURL = 'http://localhost:8080/Messages';

  value: Message[];

  constructor(private _httpClient: HttpClient) { }

  ngOnInit(): void {
    this._httpClient.get<Message[]>(this.listeMessageURL).subscribe(value => this.value = value);
  }

}
