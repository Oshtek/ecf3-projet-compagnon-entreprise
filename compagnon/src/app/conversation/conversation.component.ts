import { Conversation } from './../conversation';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.css']
})
export class ConversationComponent implements OnInit {

  // tslint:disable-next-line: variable-name
  constructor(private _httpClient: HttpClient) { }

  listeConversationURL = 'http://localhost:8080/Conversations';

  conversations: Conversation[];



  ngOnInit(): void {
    // tslint:disable-next-line: deprecation
    this._httpClient.get<Conversation[]>(this.listeConversationURL).subscribe(value => { this.conversations = value;  console.log(value); });
  }

}
