import { Message } from './message';

export class Conversation {

  constructor(){}

  // tslint:disable-next-line: variable-name
  public id: number;

  // tslint:disable-next-line: variable-name
  public message: Message[];

  // tslint:disable-next-line: variable-name
  public nbParticipant: number;


  public get Id(): number {
    return this.id;
  }

  public set Id(value: number) {
    this.id = value;
  }

  public get Message(): Message[] {
    return this.message;
  }

  public set Message(value: Message[]) {
    this.message = value;
  }

  public get NbParticipant(): number {
    return this.nbParticipant;
  }

 public set NbParticipant(value: number) {
    this.nbParticipant = value;
  }

  public toString(): string {
    return '{ \n id : ' + this.id + '; \n message: ' + this.message + '; \n nBParticipant : ' + this.nbParticipant + '; \n }';
}


}

