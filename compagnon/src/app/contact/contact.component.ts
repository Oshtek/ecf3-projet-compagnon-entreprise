import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Contact} from './../contact';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  listeContactURL = 'http://localhost:8080/Contacts';

  value: Contact[];

  constructor(private _httpClient: HttpClient) { }

  ngOnInit(): void {
    this._httpClient.get<Contact[]>(this.listeContactURL).subscribe(value => this.value = value);
  }

}
