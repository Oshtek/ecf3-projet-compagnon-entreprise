import { Conversation } from './conversation';

export class Message {

  public id: number;

  public conversation: Conversation;

  public contenu: string;

  public lu: boolean;

  public urlFichier: string;


  public get Id(): number {
    return this.id;
  }

  public set Id(value: number) {
    this.id = value;
  }

  public get Conversation(): Conversation {
    return this.conversation;
  }

  public set Conversation(value: Conversation) {
    this.conversation = value;
  }

  public get Contenu(): string {
    return this.contenu;
  }

  public set Contenu(value: string) {
    this.contenu = value;
  }

  public get Lu(): boolean {
    return this.lu;
  }

  public set Lu(value: boolean) {
    this.lu = value;
  }

  public get UrlFichier(): string {
    return this.urlFichier;
  }

  public set UrlFichier(value: string) {
    this.urlFichier = value;
  }

  public toString(): string {
    return '{ \n id : ' + this.id + '; \n conversation : ' + this.conversation + '; \n contenu : ' + this.contenu +
    '; \n contenu : ' + this.contenu + '; \n lu : ' + this.lu + '; \n urlFichier : ' + this.urlFichier + '; \n }';
}
}
